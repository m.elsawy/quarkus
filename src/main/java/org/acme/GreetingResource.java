package org.acme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.smallrye.common.annotation.NonBlocking;
import io.smallrye.mutiny.Uni;


@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello from RESTEasy Reactive";
    }
    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @NonBlocking
    public Uni<String> sayHello(String name){
        String result = "Hello " + name ;
        return Uni.createFrom().item(result);
    }   
}


